<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      WebAuth.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Request;

/**
 * Provides WebAuth functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

class WebAuth {
    
    /** @var The current API key **/    
    protected $apikey;
    /** @var The request object **/    
    protected $request;
    
    /**
     * Default Constructor
     * @return	 
     */      
    public function __construct() {
        $this->request = new Request();
    }
    
    /**
     * Determins if the web auth caller is authenticated via the API key
     * @return boolean True if authenticated, otherwise false
     */   
    public function isWebAuthAuthenticated() {
        $ok=true;
        if(!$this->validateWebAuthKey()) {
            $this->request->sendFailedAuthentication();
            $ok = false;
        }
        return $ok;
    }
    
    /**
     * Validates the supplied API key with the local version
     * @return boolean True if key is validated
     */    
    public function validateWebAuthKey() {
        return $this->getSentWebAuthKey()==$this->apikey;
    }    
    
    /**
     * Returns the API key sent in the request
     * @return string API Key
     */      
    public function getSentWebAuthKey() {
        $key=NULL;
        if(isset($_GET['apikey'])) { $key = $_GET['apikey']; }
        if(isset($_POST['apikey'])) { $key = $_POST['apikey']; }
        if($this->getGETVar('apikey')) { $key = $this->getGETVar('apikey'); }
        if($this->getPOSTVar('apikey')) { $key = $this->getPOSTVar('apikey'); }
        return $key;
    }  
    
    /**
     * Returns a specific variable associated with the request
     * @param string $key Key of variable to return
     * @return array  Variable requested (mixed)
     */      
    public function getVar($key) {
        return $this->request->getVar($key);
    }
    
    /**
     * Returns a variable by type in the request object
     * @param string $type Source type (get, post, headers)
     * @param string $key The key of the required variable
     * @return mixed The value of requested key, or NULL
     */      
    public function getVarByType($type, $key) {
        $data = $this->request->getVar(strtolower($type));
        return (isset($data[$key])) ? $data[$key] : NULL;
    }
    
    /**
     * Returns a $_GET variables from the request object
     * @param string $key The key of the required variable
     * @return mixed The value of requested key, or NULL
     */     
    public function getGETVar($key) {
        return $this->getVarByType('get', $key);
    }
    
    /**
     * Returns a $_POST variables from the request object
     * @param string $key The key of the required variable
     * @return mixed The value of requested key, or NULL
     */      
    public function getPOSTVar($key) {
        return $this->getVarByType('post', $key);
    }
    
    /**
     * Returns the origin request body from the request object
     * @return string The request body, or NULL
     */      
    public function getBODY() {
        return $this->request->getVarByType('body');
    }
    
    /**
     * Sets the bots API key that will be the basis for remote validation
     * @param string $apikey API key
     * @return	 
     */      
    public function setAPIKey($apikey) {
        $this->apikey = $apikey;
    }

    /**
     * Gets the current API key
     * @return string API Key
     */      
    public function getAPIKey() {
        return $this->apikey;
    }    
    
    
}