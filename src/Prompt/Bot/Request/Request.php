<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      Request.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Request;

/**
 * Provides Request functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

class Request {
    
    /** @var The current API key **/
    protected $apikey;
    /** @var The request object **/
    protected $request;
    
    /**
     * Default Constructor
     * @return	 
     */      
    public function __construct() {
        $this->parseInput();
    }
    
    /**
     * Determins if the remote caller is authenticated via the API key
     * @return boolean True if authenticated, otherwise false
     */      
    public function isAPIAuthenticated() {
        $ok=true;
        if(!$this->validateAPIKey()) {
            $this->sendFailedAuthentication();
            $ok = false;
        }
        return $ok;
    }
            
    /**
     * Validates the supplied API key with the local version
     * @return boolean True if key is validated
     */          
    public function validateAPIKey() {
        return $this->getSentAPIKey()==$this->apikey;
    }    
    
    /**
     * Sends a failed authentication response
     * @param boolean $exit Abort the script when set to true
     * @return	 
     */          
    public function sendFailedAuthentication($exit=true) {
        header(sprintf("HTTP/1.1 %s %s", 
                401, 
                'Unauthorized'
                ));
        echo "Unauthorized.";
        if($exit) { exit; }
    }
    
    /**
     * Sets the bots API key that will be the basis for remote validation
     * @param string $apikey API key
     * @return	 
     */          
    public function setAPIKey($apikey) {
        $this->apikey = $apikey;
    }
    
    /**
     * Gets the current API key
     * @return string API Key
     */          
    public function getAPIKey() {
        return $this->apikey;
    }
    
    /**
     * Parses the JSON sent in the body of the request
     * @return	 
     */          
    public function parseInput() {
        $headers = apache_request_headers();
        if(isset($headers['Content-Type']) && $headers['Content-Type']=='application/json') {
            $json_input = file_get_contents('php://input');
            if($json_input) {
                $vars = json_decode($json_input, true);
                if($vars) { $this->request = array_merge($_POST, $vars); }
                
                if(isset($this->request['vars']['body'])) {
                    try {
                       $this->request['vars']['body_vars'] = json_decode($this->request['vars']['body'], true); 
                    } catch (\Exception $ex) {
                    }                    
                }
            }
        }
    }
    
    /**
     * Determines if a variable in the request object exists
     * @param string $id Key of variable to check
     * @return boolean True if variable exists
     */          
    public function requestVarExists($id) {
        return (isset($this->request[$id]));
    }
    
    /**
     * Returns a variable in the request object
     * @param string $id Key of variable to return
     * @param string $default If variable is empty, return this value
     * @return	 
     */          
    public function getRequestVar($id, $default=NULL) {
        return ($this->requestVarExists($id)) ? $this->request[$id] : $default;
    }
    
    /**
     * Returns the type of message being received
     * @return string Message type (message, webauth, webhook)
     */          
    public function getMessageType() {
        return $this->getRequestVar('type');
    }
    
    /**
     * Returns the users Unique User ID (UUID) for this request
     * @return string UUID
     */              
    public function getUUID() {
        return $this->getRequestVar('uuid');
    }
        
    /**
     * Returns the API key sent in the request
     * @return string API Key
     */              
    public function getSentAPIKey() {
        $apikey = NULL;
        if(isset($_GET['Prompt-API-key'])) {
            $apikey = $_GET['Prompt-API-key'];
        } elseif(isset($_POST['Prompt-API-key'])) {
            $apikey = $_POST['Prompt-API-key'];            
        } else {
            $headers = apache_request_headers();
            $apikey =  (isset($headers['Prompt-API-key'])) ? $headers['Prompt-API-key'] : NULL;
        }
        return $apikey;
    }    
    
    
    /**
     * Returns all the slots associated with the request
     * @return array  Array of slots (mixed)
     */              
    public function getSlots() {
        return $this->getRequestVar('slots');        
    }
        
    /**
     * Returns a specific slot item associated with the request
     * @param string $id Key of slot to return
     * @return array  Slot requested (mixed)
     */              
    public function getSlot($id) {
        return (isset($this->request['slots'][$id])) ? $this->request['slots'][$id]  : NULL;
    }    
    
    /**
     * Returns all the prerequisites associated with the request
     * @return array  Array of slots (mixed)
     */              
    public function getPrerequisites() {
        return $this->getRequestVar('prerequisites');        
    }
    
    /**
     * Returns a specific prerequisite associated with the request
     * @param string $type Type of prerequisite to return (personalinfo, ..)
     * @return array  Prerequisite requested (mixed)
     */              
    public function getPrerequisite($type) {
        return (isset($this->request['prerequisites'][$type])) ? $this->request['prerequisites'][$type] : NULL;
    }    

    /**
     * Returns a specific variable associated with the request
     * @param string $key Key of variable to return
     * @return array  Variable requested (mixed)
     */    
    public function getVar($key) {
        return (isset($this->request['vars'][$key])) ? $this->request['vars'][$key] : NULL;
    }    
    
    /**
     * Returns all the variables associated with the request
     * @return array  Array of variables (mixed)
     */                  
    public function getVars() {
        return (isset($this->request['vars'])) ? $this->request['vars'] : NULL;
    }     
    
    /**
     * Returns the message associated with the request
     * @return string  Message sent
     */      
    public function getMessage() {
        return $this->getRequestVar('message');        
    }
    
    /**
     * Returns a variable by type in the request object
     * @param string $type Source type (get, post, headers)
     * @param string $key The key of the required variable
     * @return mixed The value of requested key, or NULL
     */     
    public function getVarByType($type, $key) {
        $data = $this->getVar(strtolower($type));
        return (isset($data[$key])) ? $data[$key] : NULL;
    } 
    
    /**
     * Returns a header variables from the request object
     * @param string $id The key of the required header
     * @return mixed The value of requested key, or NULL
     */     
    public function getHeaderVar($id) {
        return $this->getVarByType('headers', $id);
    }    
    
    /**
     * Returns a $_GET variables from the request object
     * This may come from the environment, or the relayed object
     * @param string $id The key of the required variable
     * @return mixed The value of requested key, or NULL
     */       
    public function getGETVar($id) {
        return (isset($_GET[$id])) ? filter_var($_GET[$id]) : $this->getVarByType('get', $id);
    }
    
    /**
     * Returns a $_POST variables from the request object
     * This may come from the environment, or the relayed object
     * @param string $id The key of the required variable
     * @return mixed The value of requested key, or NULL
     */       
    public function getPOSTVar($id) {
        return (isset($_POST[$id])) ? filter_var($_POST[$id]) : $this->getVarByType('post', $id);
    }
    
    /**
     * Returns a body  from the request object
     * @param string $id The key of the required variable
     * @return mixed The message body, or NULL
     */       
    public function getBody() {
        return $this->getVar('body');
    } 
    
    /**
     * Returns a body variable from the request object if it's JSON encoded
     * @param string $id The key of the required variable
     * @return mixed The value of requested key, or NULL
     */       
    public function getBodyVar($id) {
        return $this->getVarByType('body_vars', $id);
    }     
    
    /**
     * Returns a $_REQUEST variables from the request object
     * @param string $id The key of the required variable
     * @return mixed The value of requested key, or NULL
     */       
    public function getREQVar($id) {
        return (isset($_REQUEST[$id])) ? filter_var($_REQUEST[$id]) : NULL;
    }      
    
    
}