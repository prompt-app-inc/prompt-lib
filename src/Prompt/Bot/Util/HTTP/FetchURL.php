<?php

/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      FetchURL.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Util\HTTP;
/**
 * Provides Fetch URL functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

class FetchURL {
    
    /**
     * Fetch a given URL or file using CURL
     * @param string	$url	URL or file path
     * @param array	$params	Array of additional parameters(optional)
     * @return hash	Return stats hash including URL / file contents
     */
    public static function fetch($url, $params = array()) {

        $params = array_merge(array(
            "connect_timeout" => 20,
            "socket_timeout" => 20,
            "bind_ip" => NULL,
            "user_agent" => NULL, 'Prompt v1.0', //'''Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
            "auth_name" => NULL,
            "auth_password" => NULL,
            "post_data" => NULL,
            "cookie_file" => false,
            "cookies" => array(),
            "verbose" => false,
            "headers" => false,
            "followredirect" => false,
            "proxy_host"=>NULL,
            "proxy_port"=>8080,
            "custom_headers"=>array(),
                ), $params);

        $result = array("contents" => NULL, "ok" => true, "curlerror" => "", "curlerrorno" => NULL);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, $params['user_agent']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($params['connect_timeout']) {
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $params['connect_timeout']);
        }
        if ($params['socket_timeout']) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $params['socket_timeout']);
        }
        if ($params['bind_ip']) {
            curl_setopt($ch, CURLOPT_INTERFACE, $params['bind_ip']);
        }
        if ($params['custom_headers']) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $params['custom_headers']);
        }
        if ($params['cookie_file']) {
            curl_setopt($ch, CURLOPT_COOKIESESSION, true);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $params['cookie_file']);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $params['cookie_file']);
        }
        if ($params['cookies'] && is_array(($params['cookies']))) {
            foreach ($params['cookies'] as $key => $value) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(sprintf("Cookie: %s=%s", $key, $value)));
            }
        }
        
        if($params['proxy_host']) {
            curl_setopt($ch, CURLOPT_PROXY, $params['proxy_host']);         
            curl_setopt($ch, CURLOPT_PROXYPORT, $params['proxy_port']);                    
        }        

        if ($params['auth_name']) {
            curl_setopt($ch, CURLOPT_USERPWD, sprintf("%s:%s", $params['auth_name'], $params['auth_password']));
        }
        if ($params['post_data']) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params['post_data']);
        }

        if ($params['verbose']) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        }

        if ($params['headers']) {
            curl_setopt($ch, CURLOPT_HEADER, true);
        }

        if ($params['followredirect']) {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }

        $result['contents'] = curl_exec($ch);

        $result['curlinfo'] = curl_getinfo($ch);

        # Only partially written
        if ($params['headers']) {
            $result['cookies'] = array();
            preg_match_all('/Set-Cookie:\s*([^\n]*)/si', $result['contents'], $matches);
            foreach ($matches[1] as $match) {
                parse_str($match, $cookie);
                $result['cookies'] = array_merge($result['cookies'], $cookie);
            }
        }

        if (curl_errno($ch)) {
            $result['ok'] = false;
            $result['curlerror'] = curl_error($ch);
            $result['curlerrorno'] = curl_errno($ch);
        }
        curl_close($ch);

        return $result;
    }

}