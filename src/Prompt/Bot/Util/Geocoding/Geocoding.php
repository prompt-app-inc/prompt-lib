<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      Geocoding.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Util\Geocoding;

use Prompt\Bot\Core\PromptCore;

/**
 * Provides Geocoding functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

class Geocoding {

    /** @var API Key **/  
    protected $APIKey = '***ENTER API KEY HERE***';
    /** @var API Target URL **/  
    protected $APITargetURL = 'https://maps.googleapis.com/maps/api/geocode/json';

    /**
     * Sets the API key
     * @param string $apikey Source object to map
     * @return 
     */      
    public function setAPIKey($apikey) {
        $this->APIKey = $apikey;
    }

    /**
     * Provides an extended geolocation lookup by address   
     * @param string $str Address to search
     * @param array $params Optional parameters: strip_country
     * @return array|null
     * The return array contains:
     * - country
     * - administrative_area_level_1 [optional]
     * - administrative_area_level_2 [optional]
     * - locality [optional]
     * - route [optional]
     * - street_number [optional]
     * - formatted_address
     * - lat
     * - lng
     * - multiple_results -- boolean
     *
     * - long_name - [legacy] long name of the first item, quite random data (e.g.: street_number, route or locality)
     */
    public function getLocationInfo($str, $params=array()) {
        if (!$str)
            return NULL;

        $params = array_merge(array('strip_country'=>true), $params);

        $result = $this->fetchGeocode($str);
        if($result->status != "OK")
            return NULL;

        $geo = $result->results[0]->geometry;

        $return = [
            "country" => NULL,
            "administrative_area_level_1" => NULL,
            "administrative_area_level_2" => NULL,
            "locality" => NULL,
            "route" => NULL,
            "street_number" => NULL,

            "formatted_address" => NULL,
            "lat" => $geo->location->lat,
            "lng" => $geo->location->lng,
            "types" => NULL,
            "multiple_results" => (count($result->results) > 1),

            "long_name" => (isset($result->results[0]->address_components[0]->long_name)) ? $result->results[0]->address_components[0]->long_name : NULL,
            "zipcode" => (isset($result->results[0]->address_components[6]->short_name)) ? $result->results[0]->address_components[6]->short_name : NULL,
        ];

        $first_result = $result->results[0];
        $load_types = ["country", "administrative_area_level_1", "administrative_area_level_2",
            "locality", "route", "street_number"];

        foreach ($first_result->address_components as $address_component) {
            foreach ($address_component->types as $type) {
                if (array_search($type, $load_types) !== FALSE)
                    $return[$type] = $address_component->long_name;
            }
        }

        $return['formatted_address'] = $result->results[0]->formatted_address;

        if($params['strip_country']) {
            $return['formatted_address'] = preg_replace('/(.*), .*/','$1', $return['formatted_address']);
        }

        return $return;
    }

    /**
     * Find the lattitude and longitude of a given address
     * @param string $str Address to search
     * @param array $params Optional parameters: strip_country
     * @return array Result set of matched address
     */      
    public function getLatLng($str, $params=array()) {
        $params = array_merge(array('strip_country'=>true), $params);

        $result = $this->getLocationInfo($str, $params);
        if (!$result) return NULL;
        return [
            "lat" => $result["lat"],
            "lng" => $result["lng"],
            "long_name" => $result["long_name"],
            "formatted_address" => $result["formatted_address"]
        ];
    }

    /**
     * Fetch the result over the Geocoding API
     * @param string $str Address to search
     * @return array Search result
     */     
    public function fetchGeocode($str) {
        $return = NULL;
        $target = sprintf('%s?address=%s&key=%s', $this->APITargetURL, urlencode($str), $this->APIKey);
        $result = PromptCore::fetchURL($target);
        if($result['ok'] && $result['contents']) {
            $return = json_decode($result['contents']);
        }
        return $return;
    }

}