<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      oAuth.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Authenticators\oAuth;

/**
 * Provides oAuth functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

abstract class oAuth implements oAuthInterface {
    
    /**
     * Sets access token
     * @param string $access_token Access Token
     * @return	 
     */
    public function setAccessToken($access_token) {
        $this->access_token = $access_token;
    }    

    /**
     * Gets access token
     * @return	 string	$access_token
     */
    public function getAccessToken() {
        return $this->access_token;
    }    
    
    /**
     * Sets token type
     * @param string $token_type Token type
     * @return	 
     */    
    public function setTokenType($token_type) {
        $this->token_type = $token_type;
    }    

    /**
     * Gets access token
     * @return string	$access_token
     */
    public function getTokenType() {
        return $this->token_type;
    }   
    
    /**
     * Sets token expiry
     * @param  string	$expires_in Token expiry
     * @return	 
     */     
    public function setTokenExpires($expires_in) {
        $this->token_expires = $expires_in;
    }    

    /**
     * Gets token expiry date
     * @return	 string	$token_expires
     */
    public function getTokenExpires() {
        return $this->token_expires;
    }   
    
    
    /**
     * Sets refresh token
     * @param  string	$refresh_token Refresh token
     * @return	 
     */      
    public function setRefreshToken($refresh_token) {
        $this->refresh_token = $refresh_token;
    }    

    /**
     * Gets refresh token
     * @return	 string	$refresh_token
     */
    public function getRefreshToken() {
        return $this->refresh_token;
    }   
    
    /**
     * Sets token scope
     * @param  string	$token_scope Token scope
     * @return	 
     */     
    public function setTokenScope($token_scope) {
        $this->token_scope = $token_scope;
    }    

    /**
     * Gets token scope
     * @return	 string	$access_token
     */
    public function getTokenScope() {
        return $this->token_scope;
    }       
    
}
