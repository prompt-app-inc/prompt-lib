<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      oAuth.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Authenticators\oAuth;

/**
 * Provides oAuth functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

interface oAuthInterface {
   
    /**
     * Process the oAuth callback 
     * @param array $params Array of parameters
     */
    public function processCallback($params=array());
    /**
     * Retrieve the Authorization URL 
     * @param array $params Array of parameters
     */
    public function getAuthorizeURL($params=array());
    /**
     * Revoke a Token 
     * @param array $params Array of parameters
     */
    public function revokeToken($params=array());
    /**
     * Refresh an access token 
     * @param string $refresh_token Refresh token
     * @param array $params Array of parameters
     */
    public function refreshToken($refresh_token, $params=array());
}