<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      ResultItem.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Results;

/**
 * Provides ResultItem functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

abstract class ResultItem implements \JsonSerializable {
        
    /** @var Text message **/       
    protected $textmessage = '';
    /** @var Speech text **/       
    protected $speechtext = '';
    /** @var Images array **/       
    protected $images = array();
    /** @var Show auth URL **/       
    protected $showauthurl = false;
    /** @var Auth State string **/       
    protected $authstate = NULL;    
    /** @var Send MMS **/       
    protected $sendmms = false;
    /** @var Status code **/       
    protected $status = 'OK';
    /** @var Web hook reply **/       
    protected $webhookreply = NULL;

    /**
     * Map a previously JSON encoded object back to its true native type
     * @param object $obj Source object to map
     * @return 
     */      
    public function mapObject($obj) {
        foreach(get_object_vars($this) as $key=>$currentvalue) {
            if(isset($obj->$key)) {
                $this->$key = $obj->$key;
            }
        }
    }

    /**
     * Trim the message to it's maximum length
     * @param object $str    Message string
     * @return string   Trimmed message
     */          
    public function trimMessage($str) {
        $max_chars = 1600; // Current limit imposed by Twilio's SMS service
        return trim(substr($str,0,$max_chars));
    }
    
    /**
     * Get the text message
     * @return string   Current Text message
     */     
    public function getTextMessage() {
        return $this->textmessage;
    }

    /**
     * Set the text message
     * @param string $str Text message
     * @return
     */    
    public function setTextMessage($str) {
        $this->textmessage = $this->trimMessage($str);
        // If we don't have a speech response, lets copy it for convenience
        if(!$this->getSpeechText()) {
            $this->setSpeechText($str);
        }
    }
    
    /**
     * Get the Speech text
     * @return string   Current speech text
     */         
    public function getSpeechText() {
        return $this->speechtext;
    }

    /**
     * Set the speech message
     * @param string $str speech message
     * @return
     */    
    public function setSpeechText($str) {
        $this->speechtext = $this->trimMessage($str);
    }    

    /**
     * Callback for JSONSerializable
     * @return array Array of variables for serializing
     */     
    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }    
    
    /**
     * Add an image to the result set
     * @param ResultImage $image Image object
     * @return
     */     
    public function addImage(ResultImage $image) {
        $this->images[] = $image;
    }
    
    /**
     * Return all the images in the set
     * @return array Array of all the images in the set
     */    
    public function getImages() {
        return $this->images;
    }
    
    /**
     * Return a specific image by ordinal ID
     * @param integer $ord Ordinal ID of the image
     * @return ResultImage Image object, or NULL
     */    
    public function getImage($ord) {
        return (isset($this->images[$ord])) ? $this->images[$ord] : NULL;
    }
        
    /**
     * Return the Show Authorization URL status
     * @return boolean Show Authorization URL status
     */     
    public function getShowAuthURL() {
        return $this->showauthurl;
    }

    /**
     * Sets the Show Authorization URL status
     * @param boolean $bool Show Authorization URL status
     * @return
     */     
    public function setShowAuthURL($bool) {
        $this->showauthurl = $bool;
    }  
    
    /**
     * Return the Authorization state
     * @return string Authorization state
     */    
    public function getAuthState() {
        return $this->authstate;
    }

    /**
     * Sets the Authorization state
     * @param string $str Authorization state
     * @return
     */     
    public function setAuthState($str) {
        $this->authstate = $str;
    }      
    
    /**
     * Return the Send MMS status
     * @return boolean Send MMS status
     */     
    public function getSendMMS() {
        return $this->sendmms;
    }

    /**
     * Sets the Send MMS status
     * @param boolean $bool Send MMS status
     * @return
     */     
    public function setSendMMS($bool) {
        $this->sendmms = $bool;
    }    

    /**
     * Gets the Status code
     * @return string Status code
     */    
    public function getStatus() {
        return $this->status;
    }

    /**
     * Sets the Status code
     * @param string $str Status code
     * @return
     */    
    public function setStatus($str) {
        $this->status = $str;
    }      
    
    /**
     * Gets the Web hook reply
     * @return string Web hook reply
     */     
    public function getWebhookReply() {
        return $this->webhookreply;
    }

    /**
     * Sets the Web hook reply
     * @param string $str Web hook reply
     * @return
     */      
    public function setWebhookReply($str) {
        $this->webhookreply = $str;
    }      
}