<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      ResultImage.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Results;

/**
 * Provides ResultImage functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

class ResultImage {
    
    /** @var URL of the image object **/        
    protected $imageurl;
    /** @var Alt text of the image object **/        
    protected $alttext;

    /**
     * Sets the URL of the image
     * @param string $str Image URL
     * @return
     */     
    public function setImageURL($str) {
        $this->imageurl = $str;
    }
    
    /**
     * Gets the Image URL of the image
     * @return string Image URL
     */        
    public function getImageURL() {
        return $this->imageurl;
    }
    
    /**
     * Sets the Alt. text of the image
     * @param string $str Image alt. text
     * @return
     */       
    public function setAltText($str) {
        $this->alttext = $str;
    }
    
    /**
     * Gets the Alt. text of the image
     * @return string Image alt. text
     */    
    public function getAltText() {
        return $this->alttext;
    }
    
}
