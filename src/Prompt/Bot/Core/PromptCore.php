<?php

/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      PromptCore.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Core;

use Prompt\Bot\Util\HTTP;

/**
 * Provides Prompt Core functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

class PromptCore {
    
    /**
     * Fetch a given URL or file using CURL
     * Now factory use for legacy support
     * @param  string	$url	URL or file path
     * @param  array	$params	Array of additional parameters(optional)
     * @return	 hash	Return stats hash including URL / file contents
     */
    public static function fetchURL($url, $params = array()) {
        return HTTP\FetchURL::fetch($url, $params);
    }

    /**
     * Formats a number to global preferences. STUB: Not yet implemented with locale;
     * DO NOT use Prompt\IF YOU NEED TO MANIPULATE THIS NUMBER LATER ON DUE TO THOUSAND SEPERATORS (,) etc. INVALIDATING NUMERIC FUNCTIONS
     * @param string	$f          Number to format
     * @param array	$params     array('precision'=>Number of decimal places, 'blank'=>blank value,'thousandsep'=>thousands seperator;
     * @return	string Formatted number
     */
    public static function formatNumber($f, $params=array()) {
        $params = array_merge(array('precision'=>2,'blank' => '', 'thousandsep' => ','), $params);
        return (is_numeric($f)) ? number_format($f, $params['precision'], ".", $params['thousandsep']) : $params['blank'];
    }

    /**
     * Returns the users date  format preference. STUB: Not yet implemented with locale;
     * @param decimal $v Decimal amount	
     * @param string $blank Value to return if empty	
     * @return string Date format template
     */
    public static function formatCurrency($v, $blank) {
        $u = '$';
        $v = ($v) ? sprintf("%s%s", $u, PromptCore::formatNumber($v)) : $blank;
        return $v;
    }

    /**
     * Creates an hash based on password, secret key and optional hashing algorithm
     * @param string $string	String to hash
     * @param string $secretkey	Secret key 
     * @param string $type		Algorithm. Defaults to sha256  (i.e. "md5", "sha256", "haval160,4", etc..) 
     * @return	string	MD5 hashed string
     */
    public static function encryptToHash($string, $secretkey, $type = "sha256") {
        return hash_hmac($type, $string, $secretkey, false);
    }

    /**
     * Outputs no cache headers
     * @return
     */    
    public static function noCacheHeaders() {
        header("Cache-Control: no-cache, must-revalidate, max-age=0");
        header("Expires: Fri, 31 Dec 1998 12:00:00 GMT");
    }
    
    /**
     * Determines if a variable could be classed as 'true'
     * @param string $str	String to check
     * @return boolean true if is 'true'
     */    
    public static function isTrue($str) {
        return (strtolower($str)=='yes' || strtolower($str)=='true' || $str=="1");
    }      

}