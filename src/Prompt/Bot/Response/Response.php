<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      Response.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Response;

use Prompt\Bot\Results;

/**
 * Provides Response functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

class Response {
    
    /** @var Status code of the response **/       
    protected $statuscode = NULL;    
    
    /**
     * Default Constructor
     * @return	 
     */      
    public function __construct() {}
        
    /**
     * Sets the status code of the response
     * @param string $str Status code
     * @return
     */    
    public function setStatusCode($str) {
        $this->statuscode = $str;
    }
    
    /**
     * Returns the status code of the request
     * @return string  Status code of the request
     */      
    public function getStatusCode() {
        return $this->statuscode;
    }    
        
    /**
     * Maps a ResultItem object to the array response format required by Prompt
     * @param Results\ResultItem $suggestionitem Suggestion Item
     * @return array Mapped result item
     */     
    public function mapResultItem(Results\ResultItem $suggestionitem) {
        
        $result = array(
            'sendmms' => $suggestionitem->getSendMMS(),
            'showauthurl' => $suggestionitem->getShowAuthURL(),
            'authstate'  => $suggestionitem->getAuthState(),
            'text' => $suggestionitem->getTextMessage(),
            'speech'=>$suggestionitem->getSpeechText(),
            'status' => ($this->getStatusCode()) ? $this->getStatusCode() : $suggestionitem->getStatus(),
            'webhookreply'=>$suggestionitem->getWebhookReply(),
            'images' => array()
            );        

        foreach($suggestionitem->getImages() as $image) {
            $result['images'][] = array(
                'imageurl' => $image->getImageURL(),
                'alttext' => $image->getAltText(),
            );
        }
        
        return $result;
    }
    
    /**
     * Outputs the response to Prompt
     * @param array $data Mapped data object
     * @param array $params Supplementary paramaeters
     * @return
     */     
    public function send($data, $params = array()) {
        $params = array_merge(array('format' => 'json'), $params);
        
        $payload = '';
        
        switch($params['format']) {
            case "json":
            default:
                $payload = json_encode($data,JSON_UNESCAPED_UNICODE);
        }
        
        header('Content-Type: application/json');
        
        echo $payload;
        
    }
    
    /**
     * Bundles the mapping of a result item and outputing the response
     * @param Results\ResultItem $resultitem ResultItem object
     * @param array $params Supplementary paramaeters
     * @return
     */     
    public function reply(Results\ResultItem $resultitem, $params = array()) {
       $payload = $this->mapResultItem($resultitem);
       $this->send($payload);        
    }
    
    
}