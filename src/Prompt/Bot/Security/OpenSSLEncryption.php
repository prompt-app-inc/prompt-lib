<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      OpenSSLEncryption.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Security;

/**
 * Provides OpenSSL Encryption functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

class OpenSSLEncryption
{
    /** @var Private Key **/       
    protected $privatekey;
    /** @var Public Key **/       
    protected $publickey;
    /** @var Pass phrase **/       
    protected $passphrase;

    /**
     * Default Constructor
     * @return	 
     */      
    public function __construct()    { }
    
    
    /**
     * Loads the public key for encryption
     * @param string $filename Full file path to the public key
     * @return 
     */      
    public function loadPublicKey($filename) {
        $str = @file_get_contents($filename);
        if($str) {
            $this->setPublicKey($str);
        }
    }

    /**
     * Loads the private key for encryption
     * @param string $filename Full file path to the private key
     * @return 
     */      
    public function loadPrivateKey($filename) {
        $str = @file_get_contents($filename);
        if($str) {
            $this->setPrivateKey($str);
        }
    }    
	
    /**
     * Sets the private key for encryption
     * @param string $key Private key for encryption
     * @return 
     */          
    public function setPrivateKey($key) {
            $this->privatekey=$key;		
    }

    /**
     * Gets the private key for encryption
     * @return string Private key
     */          
    public function getPrivateKey() {
            return $this->privatekey;		
    }	

    /**
     * Sets the public key for encryption
     * @param string $key Public key for encryption
     * @return 
     */    
    public function setPublicKey($key) {
            $this->publickey=$key;		
    }
    
    /**
     * Gets the public key for encryption
     * @return string Public key
     */          
    public function getPublicKey() {
            return $this->publickey;		
    }	

    /**
     * Sets the keys passphrase for encryption
     * @param string $passphrase Key passphrase
     * @return 
     */      
    public function setPassphrase($passphrase) {
            $this->passphrase=$passphrase;		
    }
    
    /**
     * Gets the keys passphrase for encryption
     * @return string Key passphrase
     */    
    public function getPassphrase() {
            return $this->passphrase;		
    }    
    

    /**
     * Encypt a string using the public key
     * @param string $stringToEncrypt String to encrypt
     * @param boolean $base64encoded If true, base64 encode the result
     * @return string Encrypted string
     */       
    public function encryptString( $stringToEncrypt, $base64encoded = true )
    {
            $encryptedData=NULL;
            
            if($this->publickey && strlen($stringToEncrypt)<=245) {
                $key_resource = openssl_get_publickey($this->publickey);
                $ok = openssl_public_encrypt($stringToEncrypt,$encryptedData,$key_resource);                 
            } else {
                error_log('encryptString() called with string longer that 240, ignored');
            }
            return ($base64encoded) ? base64_encode($encryptedData) : $encryptedData;
    }

    /**
     * Decrypt a string using the private key
     * @param string $stringToDecrypt String to decrypt
     * @param boolean $base64encoded If true, the string has been previously base64 encoded
     * @return string Decrypted string
     */ 
    public function decryptString( $stringToDecrypt, $base64encoded = true )
    {
         $decryptedData=NULL;
         if($this->privatekey) {
            if ( $base64encoded ) {
                $stringToDecrypt = base64_decode( $stringToDecrypt );
            }
            $key_resource = openssl_get_privatekey($this->privatekey, $this->passphrase);            
            $ok = openssl_private_decrypt($stringToDecrypt,$decryptedData,$key_resource);
            #echo $decryptedData;exit;
         }
        // Return the decrypted data
         return  $decryptedData; // the rtrim is needed to remove padding added during encryption
    }
    
    
    /**
     * Encypt a string using the public key
     * @param string $stringToEncrypt String to encrypt
     * @return string Encrypted string
     */           
    public function encrypt($v) {
            return $this->encryptString($v,true);
    }

    /**
     * Decrypt a string using the private key
     * @param string $stringToDecrypt String to decrypt
     * @return string Decrypted string
     */     
    public function decrypt($v) {
            return $this->decryptString($v, true);
    }

}
