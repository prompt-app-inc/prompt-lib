<?php
/*
 * Copyright (C) 2016 Prompt App, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      ServiceConfiguration.php
 *      
 *      Part of the Prompt Sample App. Copyright Prompt App, Inc 2016.
 *      @author Prompt app. Inc.
 */

namespace Prompt\Bot\Config;

/**
 * Provides ServiceConfiguration functionality to Prompt bots.
 *
 * @author Prompt app. Inc.
 */

abstract class ServiceConfiguration {
	
        /** @var array Configuration array, one entry per each environment **/
	protected $configArr = array(
		"default"=>array(
                            "IS_DEVELOPMENT_SERVER"=>false,
                            "MYSQL_HOSTNAME"=>"",
                            "MYSQL_USERNAME"=>"",
                            "MYSQL_PASSWORD"=>"",
                            "MYSQL_DATABASE"=>"",
			),
		);

        /** @var The current HTTP host name **/
	private $http_host = "";
        /** @var The current document root **/
        private $document_root = "";
        /** @var The current configuration **/
	private static $thisConfig=array();

        /**
         * Default Constructor
         * @param string $params Array of initial parameters
         * @return	 
         */        
	public function __construct($params=array()) {
		$this->initConfig($params);
	}

        /**
         * Initializes the configuration, loading constants from various sources
         * @param string $params Array of initial parameters
         * @return	 
         */        
	public function initConfig($params=array()) {	
                $params = array_merge(array('DOCUMENT_ROOT'=>NULL), $params);
		if(!isset($_SERVER['HTTP_HOST'])) { $_SERVER['HTTP_HOST']="promptapp.io"; }
		if(!isset($_SERVER['SERVER_ADDR'])) { $_SERVER['SERVER_ADDR']="127.0.0.1"; }

		$this->http_host = preg_replace("|:[0-9]+|","", $_SERVER['HTTP_HOST']);	
                $this->document_root = $params['DOCUMENT_ROOT'];
                
                self::$thisConfig['BOT_ENVIRONMENT'] = $this->getBotEnvironment();
                
                // Look in environment
                if(!$this->document_root) {
                   $this->document_root = (isset($_SERVER['DOCUMENT_ROOT'])) ? $_SERVER['DOCUMENT_ROOT'] : NULL;
                }
                
                // Fall back to file path
                if(!$this->document_root) {
                    $this->document_root = (sprintf('%s',preg_replace('|/include.*|','',__DIR__)));
                }
                
                $config = array();
                
                if(!$config) {
                    $config = $this->loadDotEnv();
                }
                
                if(!$config && isset($this->configArr[$this->http_host])) {
                    $config = $this->configArr[$this->http_host];
                    if(isset($config['alias']) && isset($this->configArr[$config['alias']])) {
                        $config = $this->configArr[$config['alias']];
                    }                    
                }
                
                if(!$config && isset($this->configArr[$_SERVER['SERVER_ADDR']])) {
                    $config = $this->configArr[$_SERVER['SERVER_ADDR']];
                }                
                
                if(!$config && isset($this->configArr[$_SERVER['DOCUMENT_ROOT']])) {
                    $config = $this->configArr[$_SERVER['DOCUMENT_ROOT']];
                }                
                
                self::$thisConfig = array_merge(self::$thisConfig,$config);
                                
		if(!isset(self::$thisConfig['MYSQL_HOSTNAME']) || is_null(self::$thisConfig['MYSQL_HOSTNAME'])) {
			self::$thisConfig = array_merge(self::$thisConfig,$this->configArr['default']);
		}	

		$this->injectEnvironmentVars();
                
	}
        
        /**
         * Returns the current Bot Environment
         * @return  string  The current bot ennvironment (localhost, dev, prod, ..)	 
         */        
        public function getBotEnvironment() {
            return (isset($_SERVER['BOT_ENVIRONMENT'])) ? $_SERVER['BOT_ENVIRONMENT'] : 'localhost';
        }
        
        /**
         * Load appropriate .env_ file from the document root 
         * @return  array   Array of configuration variables
         */        
        public function loadDotEnv() {
            $env_file = sprintf('%s/.env_%s', $this->document_root, $this->getBotEnvironment());
            $config = array();
            if(file_exists($env_file)) {
                foreach(file($env_file) as $line) {
                    $bits = preg_split('/=/', $line);
                    $key = (isset($bits[0])) ? trim($bits[0]) : NULL;
                    $value = (isset($bits[1])) ? trim($bits[1]) : NULL;
                    if($key && $value) {
                        switch($value) {
                            case 'true':
                                $value = true;
                                break;
                            case 'false':
                                $value = false;
                                break;
                        }
                        $config[$key] = $value;
                    }
                }                
            }
            return $config;
        }
        
        /**
         * Injects environment variables in to the configuration
         * @return	 
         */        
	public function injectEnvironmentVars() {
		foreach(array("MYSQL_HOSTNAME","MYSQL_HOSTNAME_READONLY_1","MYSQL_USERNAME","MYSQL_PASSWORD","MYSQL_DATABASE","MYSQL_SECURE_HOSTNAME","MYSQL_SECURE_USERNAME","MYSQL_SECURE_PASSWORD","MYSQL_SECURE_DATABASE") as $v) {
			if(isset($_SERVER[$v])) { self::$thisConfig[$v]=$_SERVER[$v]; }
		}
	}

	/**
	* Returns a configuration value
	* @param string	$key Configuration key
	* @return string	Configuration value
	*/	
	public static function getValue($key) {
		return (isset(self::$thisConfig[$key])) ? self::$thisConfig[$key] : NULL;
	}
        
        /**
         * Returns a tenant specific value (obsolete)
         * @param string $tenantid Tenant ID
         * @param string $key Key of value to be retrieved
         * @return string Tenant value	 
         */        
        public static function getTenantValue($tenantid, $key) {
            $value = NULL;
            $tenantHash = self::getValue('tenants');
            
            if($tenantHash) {
                $value = (isset($tenantHash[$tenantid][$key])) ? $tenantHash[$tenantid][$key] : NULL;
            }
            return $value;
        }
	
}